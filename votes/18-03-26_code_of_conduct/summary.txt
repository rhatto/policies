=====================================================================
Summary
=====================================================================

Topic: Code of Conduct and Statement of Values
Date taken: 18/3/26 - 18/4/8
Vote count: 32 (of 92 eligible voters, 35%)

Secretary: Karsten Loesing
Proposer: Alison Macrina

Topic put to a vote were...

  Question 1: Code of Conduct

    Please choose one of the following options for the *Code of Conduct*:

    a. I approve of the proposal.

    b. I do not approve of the proposal.

    c. I abstain from this vote.

  Question 2: Statement of Values

    Please choose one of the following options for the *Statement of Values*:

    a. I approve of the proposal.

    b. I do not approve of the proposal.

    c. I abstain from this vote.

Results were...

  * Code of Conduct passes. 97% approved of its adoption.

  * Statement of Values passes. 93% approved of its adoption.

=====================================================================
Votes
=====================================================================

Alison Macrina
Allen Gunn
Antonela Debiasi
Colin Childs
Damian Johnson
David Goulet
Georg Koppen
George Kadianakis
hiro
Ian Goldberg
isabela
isis
kat
Kathleen Brade
Mark Smith
Matt Traudt
Matthew Finkel
micah
Mike Perry
Moritz Bartl
Nick Mathewson
Richard Pospesel
Rob Jansen
Roger Dingledine
Sukhbir Singh
Taylor Yu
teor
Tom Ritter
Tommy Collison
Vasilis
Wendy Seltzer
Yawning Angel

419;A;A
1087;A;A
1494;A;A
1857;A;A
2535;A;A
4448;A;A
6439;A;A
7205;A;A
8458;A;A
11777;A;A
12015;A;A
12492;A;A
12810;A;A
14641;A;A
14681;A;A
15047;<abstain>;<abstain>
15429;A;A
15887;A;B
16362;A;A
17747;A;A
18248;A;A
19022;A;A
21773;A;A
23496;<abstain>;<abstain>
24887;B;B
26266;A;A
26510;A;A
26754;A;A
29447;A;A
30953;A;A
31419;A;A
31717;A;A

=====================================================================
Question 1: Code of Conduct
=====================================================================

Approve: 29 (97%)
Reject: 1 (3%)

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

Motion passes. We'll adopt this as our code of conduct.

=====================================================================
Question 2: Statement of Values
=====================================================================

Approve: 28 (93%)
Reject: 2 (7%)

---------------------------------------------------------------------
Result:
---------------------------------------------------------------------

Motion passes. We'll adopt this as our statment of values.

