# Tor Community Council

To facilitate harmonious collaboration within our internal community, the
Community Council is an elected body with the following responsibilities...

  * mediation of intra-community disputes
  * enforcement of enacted policies like the code of conduct
  * provide Tor Project Inc with community representation it can consult

The council's jurisdiction includes the set of people on our official
communication channels (email lists, irc, trac, developer meetings, etc) as
well as those places themselves. The Community Council has co-authority with
the Tor Project Inc. (TPI) with the exception regarding Tor Project volunteers,
who are not subject to TPI's authority.

## Who is on the Community Council?

There are five members on the Community Council, with membership an elected
position of a one year term (from April 1st to March 31st). Election of council
members is conducted as follows...

* Vote is started with a one week call for volunteers. Any tor-internal@ member
  interested in having this responsibility can add themselves to the running by
  writing a few sentences to tor-internal@ about why they'd like to join the
  Community Council.

* This is then conducted as a standard two week vote with two questions...

  1. List up to five of these individuals you'd like to see on the new
     Community Council.

  2. Would you be uncomfortable having any of these people adjudicate your
     issues?

If at least 1/4 of voters name a person in the second question they're dropped
from the running. The top five individuals become the Community Council.

If we lack five acceptable volunteers for the new council then the council size
can be dropped to four or three.

If we lack even three acceptable volunteers the Community Council is suspended
until more volunteers step forward.

Council members have a term limit of five consecutive years, after which they
must take at least a full year off. Partial terms (being appointed or ending
mid-year) counts as being part of the council for the whole year.

The Community Council needs to represent the breadth of the Tor community to
function effectively. As such, the Community Council must include at least one
female-identified person. If we lack a female-identified person before the
councilmembers are voted on, the vote can be delayed up to one month and
another call for volunteers will be sent out. 

We recognize the need for other kinds of voices that were not well represented
in our community at the time these guidelines were ratified -- namely, a
diverse set of racial, geographic, linguistic, and technical backgrounds. Since
we currently lack enough current contributors to make representation of those
identities a requirement for this council, we will acknowledge here the
priority of getting better representation in our community as a whole. 

## Can someone be removed from the Community Council?

Any tor-internal@ member that feels uncomfortable with our Community Council
may request an early election of a new council. Council members themselves
might do this if they feel one of their number needs to be replaced (for
instance due to ignoring conflicts of interest or behaving unethically).

Early council elections will be conducted if requested by five tor-internal@
members. If a new vote is conducted none of these five individuals can request
another early vote until the term of the new council expires.

## How can I contact the Community Council?

The Community Council can be contacted at...

  tor-community-council@torproject.org

Members of the council will make their contact details and PGP keys available
on the wiki along with copies of public decisions. The wiki address is here:

  https://gitlab.torproject.org/tpo/team/-/wikis/Community-Council

To request for the council to get involved with a dispute contact the above
list or any of its members. Complaints should include...

  * Names of the individuals the complaint is related to.
  * Details of the specific issue.
  * Relevant parts of the community governance documents if applicable.
  * Offer ideas for resolution.

  * Can this issue be discussed openly on tor-internal@?
  * Can this issue be discussed with Tor Project Inc leadership?
  * Can information about this issue be public (ie. posted on tor-project@ and
    the wiki afterward)?

The council must honor requests for matters to be private. If they feel they
cannot do so (for instance they're being barred from talking with TPI but
properly handling the complaint would require them to do so) then they may
refuse to handle the issue unless that restriction is lifted.

If the complainant wishes, they may withdraw their complaint at any time during
the investigation.

## Community Council procedures

The Community Council will contact relevant parties then confer at their
discretion (either privately or with the relevant parties) on how best to
resolve the situation. All councilmembers involved in resolving a particular
issue must be involved in council communication on that issue (no
backchanneling).

Council members must recuse themselves if named in the complaint or there is a
conflict of interest identified by at least five community members.

If this is a lengthy issue the council will provide weekly updates to all
parties involved.

## Can decisions be appealed?

Yes. Decisions can be appealed with the Community Council. If individuals reach
an impasse with the council they can appeal the issue with Tor Project Inc.

## What authority does the Community Council have?

The Community Council acts not only as a mediator in intra-community disputes
but to provide our volunteer community with an elected voice the Tor Project
Inc can consult. In practice topics are expected to fall into two camps...

* Topics that are neither confidential nor time sensitive will be put forward
  to our tor-internal@ community at large. The council's role here is to lead
  the discussion, and possibly conduct a brief vote or make a decision if the
  community cannot come to a timely consensus.

* For topics that cannot be put forward to the wider community the council will
  deliberate among themselves.

Some examples of questions Tor Project Inc might defer to the council on are
things like...

* Should this person be excluded from the upcoming dev meeting?

* Should email list membership of a person be revoked?

Either Tor Project Inc or the Community Council can grant, suspend, or revoke
tor-internal@ membership, with the other party acting as a point of appeal.
Please note that the council cannot handle issues with legal ramifications,
such as embezzlement. Those issues must be referred to TPI instead.

## How will decisions be announced?

All public Community Council decisions and appeals will be posted on
tor-project@ and archived on the wiki. Private decisions will be posted on
tor-internal@ unless the nature of the issue requires it to be confidential.

The Community Council must keep a public issue register listing each issue. For
public issues the register include its status, description, parties involved,
and relevant dates (initial contact, decision, appeal, and last status update).

If the issue is confidential an issue number may be used instead, with the
council and parties involved deciding what information it can safely provide
and when it should be released.

